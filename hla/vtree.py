import numpy as np


'''
Class for building typing trees.
Check the paper for some idea about how this works (or should work).
'''

class Node:
    def __init__(self, typing='.', vote=0, voter=None):
        self.typing = typing
        self.vote = vote
        self.votedby = [] if not voter else voter if type(voter)==list else [voter]
        self.children = []

    ''' Add typing under this node. Input t is a vector (i.e. split
        from for example 01:02:03 to ['01', '02', '03']). '''
    def addSubTyping(self, t, voter, voteweight=1):
        if t:
            ctypings = [x.typing for x in self.children]
            if t[0] in ctypings:
                c = self.children[ctypings.index(t[0])]
            else:
                c = Node(t[0])
                self.children.append(c)
            c.vote += voteweight
            c.votedby.append(voter)
            c.addSubTyping(t[1:], voter, voteweight)

    ''' Add nodes and votes in tree T to self. '''
    def addTree(self, T):
        if T.children:
            ctypingsSelf = [x.typing for x in self.children]
            for Tc in T.children:
                if Tc.typing in ctypingsSelf:
                    c = self.children[ctypingsSelf.index(Tc.typing)]
                else:
                    c = Node(Tc.typing)
                    self.children.append(c)
                c.vote += Tc.vote
                c.votedby.extend(Tc.votedby)
                c.addTree(Tc)

    ''' Subtract tree T from self. Note that nodes are not removed, only
        votes are subtracted. '''
    def subtrTree(self, T):
        if T.children:
            ctypingsSelf = [x.typing for x in self.children]
            for Tc in T.children:
                if Tc.typing in ctypingsSelf:
                    c = self.children[ctypingsSelf.index(Tc.typing)]
                    c.vote -= Tc.vote
                    for vtb in Tc.votedby:
                        if vtb in c.votedby:
                            c.votedby.remove(vtb)
                c.subtrTree(Tc)

    ''' Adds one allele of a G group to the tree. Doesn't upvote any nodes and
        sets vote=1 for all new nodes. '''
    def addGgroupAllele(self, t, voter, vote=1):
        if t:
            ctypings = [x.typing for x in self.children]
            if t[0] in ctypings:
                c = self.children[ctypings.index(t[0])]
            else:
                c = Node(t[0], vote=vote, voter=voter)
                self.children.append(c)
            c.addGgroupAllele(t[1:], vote=vote, voter=voter)

    ''' gcodes is the dict of all G codes and gallele is the G group to be added. '''
    def addGgroupVote(self, gcodes, gallele):
        # create temporary voting tree with vote=1 in each node
        tmptree = Node('',0)
        for g in gcodes[gallele]:
            t = g.split(':')
            tmptree.addGgroupAllele(t, voter=gallele, vote=1)
        # add temp tree to self
        self.addTree(tmptree)

    def getHighestVotedChild(self, returnFirst=False):
        if not self.children: # at a leaf node
            return None
        v = [c.vote for c in self.children]
        return (self.children[v.index(max(v))], max(v))
        return None

    def getMajorityVotedType(self, minreqvote):
        hvc = self.getHighestVotedChild()
        if hvc is None or hvc[1]<minreqvote:
            # at a leaf node or votes of all children equal or vote not high enough
            return [self.typing], self.vote
        v = [c.vote for c in self.children]
        v.remove(max(v))
        if v:
            mrv = max(max(v)+1, minreqvote)
        else:
            mrv = minreqvote
        majtyp, majvot = hvc[0].getMajorityVotedType(mrv)
        return [self.typing]+majtyp, min(self.vote, majvot)

    def getAndSubtrMajorityVotedType(self, minreqvote=0, gcodes=None, rootnode=True):
        if rootnode:
            # in the first round (=root node) allow ties in votes
            hvc = self.getHighestVotedChild(returnFirst=True)
        else:
            hvc = self.getHighestVotedChild()
        if hvc is None or hvc[1]<minreqvote:
            if rootnode:
                return self.typing,self.vote
            # at a leaf node or votes of all children equal or vote not high enough
            return self, [self.typing]
        mnode, majtyp = hvc[0].getAndSubtrMajorityVotedType(minreqvote, rootnode=False)
        # if not in root node, return the found majority voted node
        if not rootnode:
            # also prepend and return the typing info
            return mnode, [self.typing]+majtyp
        majvot = mnode.vote
        # when back in root node, form a temp voting tree to subtract
        # majority voted node's votes from self
        tmptree = Node('',0)
        for r in mnode.votedby:
            t = r.rstrip('G').split(':')
            if r[-1]=='G':
                # for all alleles in the G-group, add a special "spanning" voting tree
                tmptree.addGgroupVote(gcodes, r)
            else:
                tmptree.addSubTyping(t, voter=r)
        self.subtrTree(tmptree)
        return ':'.join(majtyp), majvot

    def getMajorityVotedTypes(self, vlist=[], typpref='', minreqvote=1):
        v = [c.vote>=minreqvote for c in self.children]
        if not any(v):
            vlist.append((typpref+':'+self.typing if typpref else self.typing,
                self.vote))
            return
        for c in self.children:
            if c.vote>=minreqvote:
                c.getMajorityVotedTypes(vlist, typpref+':'+self.typing
                        if typpref else self.typing, minreqvote)


'''
Note: here gcodes needs to be only for a given gene.
'''
def getMaxVoted(rl, minreqvoteprop, gcodes=None):
    vtree = Node('',0)
    maxvote = 0
    # form voting tree
    for r in rl:
        #TODO: should nan really be interpreted as a vote for missing allele? Interpretation
        #   of missing result may vary between programs so it's complicated...
        if not r or not any(r):
            # skip empty (i.e. no typing result from a program)
            continue
        if r==('',''):
            # empty allele pairs might come from ATHLATES results when there is no
            # common prefix at all, check here so that maxvote won't be increased in this case
            continue
        maxvote += 1
        for i in range(len(r)): # counts homozygotes twice
            if not r[i]:
                # empty alleles might come from ATHLATES results when there is no
                # common prefix for another allele
                continue
            if type(r[i])==float and np.isnan(r[i]):
                # vote for a missing allele
                vtree.addSubTyping('-', voter='-')
                continue
            t = r[i].rstrip('G').split(':')
            if r[i][-1]=='G':
                # for all alleles in the G-group, add a special "spanning" voting tree
                vtree.addGgroupVote(gcodes, r[i])
            else:
                vtree.addSubTyping(t, voter=r[i])
    minreqvote = np.ceil(minreqvoteprop*maxvote)
    # check only nodes with at least minreqvote proportion of votes
    maxv = []
    while True:
        # get types as long as they're above minreqvote
        m = vtree.getAndSubtrMajorityVotedType(minreqvote, gcodes)
        if not m[1]:
            break
        maxv.append(m)
    return maxv

