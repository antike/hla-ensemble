import os, glob
import pandas as pd
import numpy as np
import csv
import vtree


# the file used was from http://hla.alleles.org/wmda/hla_nom_g.txt
gcodesfile = "/users/whoever/HLA/hla_nom_g.txt"

'''
Read G codes.
'''
def readGcodes(filename):
    gdict = {}
    for lne in open(filename):
        if lne[0]=='#':
            continue
        if lne.rstrip()[-1]=='G':
            t = lne.rstrip().split(';')
            gdict.setdefault(t[0][:-1], {})[t[-1]] = t[1].split('/')
    return gdict


'''
Reads HLAssign results from a file.
'''
def readHLAssignResults(fname):
    hlassignres = pd.read_excel(fname, skiprows=1, na_values=['no call'])
    hlassign = {}
    for r in hlassignres.iterrows():
        if type(r[1][3])==float and np.isnan(r[1][3]):
            #NOTE: missing -> no allele
            hlassign.setdefault(r[1][0], {})[r[1][2]] = [np.nan]
        else:
            hlassign.setdefault(r[1][0], {})[r[1][2]] = [x.split('*')[1] for
                    x in r[1][3].split('/')]
    return hlassign


'''
Reads HLAreporter results from files in several folders named by sample IDs.
'''
def readHLAreporterResults(foldername):
    out = {}
    out1pair = {}
    warnings = {}
    gcodes = readGcodes(gcodesfile)
    for fldr in glob.glob(os.path.join(foldername,'*')):
        sampleid = os.path.split(fldr)[1]
        out[sampleid] = {}
        for f in glob.glob(os.path.join(fldr,'*_report.out')):
            g = os.path.split(f)[1].split('_')[2]
            alleles = []
            alldet = []
            allpair = None
            inalleles = inallelepair = inalldet = False
            for lne in open(f):
                if not lne.strip():
                    inalleles = inallelepair = inalldet = False
                    continue # skip empty lines
                if lne.startswith("Allele pair"):
                    allpair = []
                    inalleles = inalldet = False
                    inallelepair = True
                    continue
                if lne.startswith("Alleles detected"):
                    # this seems to be for class I genes
                    alldet.append([])
                    alli = len(alldet)-1
                    inalleles = inallelepair = False
                    inalldet = True
                    continue
                if lne.startswith("Allele"):
                    # this seems to be for class II genes
                    alleles.append([])
                    alli = len(alleles)-1
                    inallelepair = inalldet = False
                    inalleles = True
                    continue
                if '*' in lne and ':' in lne:
                    if inallelepair: # a "good" (i.e. unambiguous) typing result
                        allpair.append(lne.strip().split('*')[1])
                    elif inalldet: # class I gene
                        alldet[alli].append(lne.strip().split('*')[1])
                    elif inalleles: # class II gene
                        alleles[alli].append(lne.strip().split('*')[1])
                    else:
                        inalleles = inallelepair = inalldet = False
                else:
                    inalleles = inallelepair = inalldet = False
                if "WARNING" in lne:
                    warnings.setdefault(sampleid,{})[g] = lne.strip()
            if allpair is not None:
                # the "best" case (for class I genes), i.e. only one reported pair
                out[sampleid][g] = alleles
                if not len(allpair):
                    aa = [np.nan]
                else:
                    aa = allpair if len(allpair)==2 else allpair+allpair
            elif len(alleles)==0 and len(alldet)==0:
                # no reported allele (or allele pair) interpreted as missing allele
                out[sampleid][g] = [np.nan]
                aa = [np.nan]
            if allpair is None and (len(alldet) or len(alleles)):
                alls = alldet if len(alldet) else alleles
                out[sampleid][g] = alls
                if len(alls)==1 and len(alls[0])==1:
                    aa = alls[0] + alls[0]
                elif len(alls)==1:
                    atmp = [[x] for x in alls[0]]
                    # reduce list of inferred pairs to just one pair
                    # --try first with a lower threshold on 40.1%
                    mxvt = vtree.getMaxVoted(atmp, 0.401, gcodes[g])
                    thr = 0.401
                    while len(mxvt)>2:
                        # try reducing to just one or two if there were more
                        # than two voted alleles
                        thr += 0.1
                        mxvt = vtree.getMaxVoted(atmp, thr, gcodes[g])
                        if len(mxvt)<=2:
                            break
                    if not mxvt:
                        aa = ('', '') # HLAreporter can't make its mind
                    elif len(mxvt)==1:
                        if mxvt[0][1]>len(atmp):
                            # this is a homozygote call
                            aa = (mxvt[0][0], mxvt[0][0])
                        else:
                            aa = (mxvt[0][0], '')
                    else:
                        aa = (mxvt[0][0], mxvt[1][0])
                else:
                    aa = []
                    for a1 in alls:
                        if len(a1)==1:
                            aa.append(a1[0])
                            continue
                        # reduce list of inferred pairs to just one allele
                        # --try first with a lower threshold on 40.1%
                        atmp = [[x] for x in a1]
                        mxvt = vtree.getMaxVoted(atmp, 0.401, gcodes[g])
                        thr = 0.401
                        while len(mxvt)>1:
                            # try reducing to just one
                            thr += 0.1
                            mxvt = vtree.getMaxVoted(atmp, thr, gcodes[g])
                            if len(mxvt)<=1:
                                break
                        if not mxvt:
                            aa.append('') # HLAreporter can't make its mind
                        else:
                            aa.append(mxvt[0][0])
            out1pair.setdefault(sampleid, {})[g] = aa
    return out, warnings, out1pair
                    

'''
Read ATHLATES results.
'''
def readATHLATESresults(folder):
    out = {}
    out1pair = {}
    for fldr in glob.glob(folder+'/*'):
        samp = fldr.split('/')[-1]
        for fname in glob.glob(fldr+'/*typing.txt'):
            g = fname.split('_')[1].split('.')[0]
            out.setdefault(samp, {})[g] = []
            f = open(fname)
            inInfAllP = False
            for lne in f:
                if "Inferred Allelic Pairs" in lne:
                    inInfAllP = True
                    continue
                if not inInfAllP:
                    continue
                if lne.rstrip(): # a non-empty line
                    t = lne.rstrip().split('\t')
                    a1 = t[0].split('*')[1]
                    a2 = t[2].split('*')[1]
                    # add allele call to either 1st or 2nd position depending on
                    # which has a longer common prefix and thus "fits better" with
                    # the others
                    ord1s = (len(os.path.commonprefix([x[0] for x in out[samp][g]]+[a1])) +
                            len(os.path.commonprefix([x[1] for x in out[samp][g]]+[a2])))
                    ord2s = (len(os.path.commonprefix([x[0] for x in out[samp][g]]+[a2])) +
                            len(os.path.commonprefix([x[1] for x in out[samp][g]]+[a1])))
                    if ord1s>ord2s:
                        out[samp][g].append([a1,a2])
                    else:
                        out[samp][g].append([a2,a1])
            f.close()
            # reduce list of inferred pairs to just one pair
            if not out[samp][g]: # NOTE: no call at all, i.e. a missing allele(?)
                aa = [np.nan]
            elif len(out[samp][g])==1:
                # strip some possible chars from end
                aa = map(lambda x: x.rstrip('LNG'), out[samp][g][0])
            else:
                # try first with a lower threshold on 40.1%
                mxvt = vtree.getMaxVoted(out[samp][g], 0.401)
                thr = 0.401
                while len(mxvt)>2:
                    # try reducing to just one or two if there were more
                    # than two voted alleles
                    thr += 0.1
                    mxvt = vtree.getMaxVoted(out[samp][g], thr)
                    if len(mxvt)<=2:
                        break
                if not mxvt:
                    aa = ('', '')#np.nan # ATHLATES can't make its mind
                elif len(mxvt)==1:
                    if mxvt[0][1]>len(out[samp][g]):
                        # this is a homozygote call
                        aa = (mxvt[0][0], mxvt[0][0])
                    else:
                        aa = (mxvt[0][0], '')
                else:
                    aa = (mxvt[0][0], mxvt[1][0])
            out1pair.setdefault(samp, {})[g] = aa
    return out,out1pair


'''
Read bwakit results.
'''
def readBwakitResults(filename):
    out = {}
    f = open(filename)
    for lne in f:
        t = lne.split('\t')
        samp = t[0].split('/')[1]
        g = t[1].split('*')[0][4:]
        out.setdefault(samp, {})[g] = [t[1].split('*')[1], t[2].split('*')[1]]
    f.close()
    return out


'''
Reads Omixon Target results from a tsv file.
'''
def ReadOmixonResults(fname):
    with open (fname) as tsvfile:
        omixon = {}
        reader = csv.reader(tsvfile, delimiter = '\t')
        header = next(reader) # read header line
        for gene_name in range(2,len(header)): # Modify HLA-A and HLA-B gene names  etc. to just A, B, ...
            if '-' in header[gene_name]:
                header[gene_name] = header[gene_name].split('-')[1]
            else:
                continue
        sampleID = ''
        allele = ''
        genotype1 = {}
        genotype2 = {}
        for row in reader:
            if row[0] == sampleID:
                if row[1] == allele:
                    continue # take only the best prediction for each allele
                else: # record the second allele
                    for gene in range(2,len(header)): # go through all genes
                        if row[gene] == '': # no call
                            continue # no second allele if no first allele has been called
                        else:
                            allele2 = row[gene].split('*')[1]
                            allele2 = allele2.split(' ')[0]
                            genotype2[header[gene]] = allele2
                            omixon.setdefault(row[0],{})[header[gene]] = [genotype1[header[gene]], genotype2[header[gene]]]
                    allele = row[1]
            else: # process next sample
                for gene in range(2,len(header)): # go through all genes
                    if row[gene] == '': # no call
                        #NOTE: no call -> missing allele
                        omixon.setdefault(row[0],{})[header[gene]] = [np.nan]
                    else: # record first allele
                        allele1 = row[gene].split('*')[1]
                        allele1 = allele1.split(' ')[0]
                        genotype1[header[gene]] = allele1

                sampleID = row[0]
                allele = row[1]
        return omixon


''' Read OptiType results. Note: reads all the folders within the given folder so
    every entry there should be a valid Optitype result folder. '''
def readOptiTypeResults(folder):
    out = {}
    for fldr in glob.glob(folder+'/*'):
        samp = fldr.split('/')[-1]
        fname = glob.glob(os.path.join(glob.glob(os.path.join(fldr, '*'))[0],
            '*result.tsv'))[0]
        f = open(fname)
        f.readline() # skip header
        toks = f.readline().rstrip().split('\t')
        for t in toks:
            if t[0] in ['A', 'B', 'C'] and t[1]=='*':
                g = t[0]
                out.setdefault(samp, {}).setdefault(g, []).append(t[2:])
        f.close()
    return out


''' Read HLA typing results done for 1000 genomes samples (from NNN NNN et al.) '''
def read1kbTypings(fname):
    out = {}
    f = open(fname)
    hdr = map(lambda x: x.strip('"').split('.')[0], f.readline().rstrip().split())
    for lne in f:
        t = map(lambda x: x.strip('"'), lne.rstrip().split())
        samp = t[0]
        for i in range(2,len(t)):
            out.setdefault(samp, {}).setdefault(hdr[i], []).append(t[i].split('/'))
    f.close()
    return out


