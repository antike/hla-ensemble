Basic scripts for doing HLA typing from HLA NGS data using the ensemble approach discussed in https://dx.doi.org/10.3389%2Ffimmu.2017.01815
Contact antti.larjo@gmail.com in case of ideas, questions, etc.
