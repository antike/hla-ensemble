"""
An example script for running the ensemble HLA typing.
"""

import hla.hlaReaders as hlaReaders
import hla.vtree as vtree


'''
Combine results from different typings by majority voting.
Input rlist must be a list of dicts, where each dict is the results
from one typing method.
Output is a tuple: (the majority voting winner, proportion of votes)
In case of no winner, output is '.'
Winning typing resolution is given with as high detail as agreed by voters.
'''
def ensembleResults(rlist, minreqproportion=0.5001, gcodes=None):
    sampids = set()
    glist = set()
    for r in rlist:
        sampids = sampids.union(r.keys())
        for rk in r:
            glist = glist.union(r[rk].keys())
    sampids = sorted(sampids)
    glist = sorted(glist)
    # Note: -in voting, missing alleles give only one vote (and thus are not considered
    #   similarly to homozygous calls where they give two votes), some rationale
    #   for this is that the missing "call" might actually be in some programs just
    #   inability to give a typing result for some other reasons
    ag = {}
    for t in sampids:
        for g in glist:
            rl = [d.get(t, {}).get(g, []) for d in rlist]
            mv = vtree.getMaxVoted(rl, minreqproportion, gcodes.get(g,None)
                    if gcodes else None)
            if mv and mv[0][0]:
                if len(mv)>2:
                    # can happen in some (homozygous) cases, that there are
                    # more than two left here, but the last one is likely only
                    # a "remnant" of the first ones, i.e. "02" from "02:01" etc.
                    if mv[0][0].startswith(mv[-1][0]) or mv[1][0].startswith(mv[-1][0]):
                        mv.pop(-1)
                ag.setdefault(t,{})[g] = ["{}({:.2f})".format(x[0],x[1])
                        for x in mv]
            else:
                ag.setdefault(t,{})[g] = '.'
    return ag


# read HLAassign result file
hlassign = hlaReaders.readHLAssignResults('HLAssign_results.xlsx')
# read HLAreporter results
allhlareporter, hlarepWarnings, hlareporter = hlaReaders.readHLAreporterResults('HLAreporter_results')
# read ATHLATES results
allathlres,athlres = hlaReaders.readATHLATESresults("ATHLATES_res")
# read omixon results
omixon_results = hlaReaders.ReadOmixonResults("Omixon_results.txt")
# read OptiType results
optityperes = hlaReaders.readOptiTypeResults("OptiType")

gcodes = hlaReaders.readGcodes("hla_nom_g.txt")

# create list of union of genes
k = hlassign.keys()[0]
glist = sorted(set(hlassign[k].keys()).union(hlareporter[k].keys()).union(
        athlres[k].keys())
resList = [hlassign, hlareporter, athlres, omixon_results, optityperes]
# append voting results. minreqproportion is the minimum required proportion
# of votes needed for an allele to be reported (max num of votes is 2*resList as each
# typing method is considered to have two votes)
ensres = ensembleResults(resList, minreqproportion=0.499, gcodes=gcodes)
